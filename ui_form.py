# -*- coding: utf-8 -*-

################################################################################
## Form generated from reading UI file 'form.ui'
##
## Created by: Qt User Interface Compiler version 6.7.0
##
## WARNING! All changes made in this file will be lost when recompiling UI file!
################################################################################

from PySide6.QtCore import (QCoreApplication, QDate, QDateTime, QLocale,
    QMetaObject, QObject, QPoint, QRect,
    QSize, QTime, QUrl, Qt)
from PySide6.QtGui import (QBrush, QColor, QConicalGradient, QCursor,
    QFont, QFontDatabase, QGradient, QIcon,
    QImage, QKeySequence, QLinearGradient, QPainter,
    QPalette, QPixmap, QRadialGradient, QTransform)
from PySide6.QtWidgets import (QApplication, QComboBox, QGridLayout, QHBoxLayout,
    QLabel, QPushButton, QSizePolicy, QSpacerItem,
    QVBoxLayout, QWidget)
import rc_rc_img

class Ui_Widget(object):
    def setupUi(self, Widget):
        if not Widget.objectName():
            Widget.setObjectName(u"Widget")
        Widget.resize(800, 400)
        Widget.setMinimumSize(QSize(800, 400))
        Widget.setMaximumSize(QSize(800, 400))
        icon = QIcon()
        icon.addFile(u":/mini-vantage/mini-vantage.png", QSize(), QIcon.Normal, QIcon.Off)
        Widget.setWindowIcon(icon)
        Widget.setStyleSheet(u"background-color: rgb(32, 61, 146);\n"
"color: rgb(255, 255, 255);")
        self.gridLayout = QGridLayout(Widget)
        self.gridLayout.setSpacing(20)
        self.gridLayout.setObjectName(u"gridLayout")
        self.gridLayout.setContentsMargins(20, 20, 20, 20)
        self.widget_2 = QWidget(Widget)
        self.widget_2.setObjectName(u"widget_2")
        self.widget_2.setMinimumSize(QSize(0, 0))
        self.widget_2.setMaximumSize(QSize(200, 200))
        self.widget_2.setStyleSheet(u"background-color: rgb(59, 85, 175);\n"
"border-radius: 20px;")
        self.verticalLayout = QVBoxLayout(self.widget_2)
        self.verticalLayout.setObjectName(u"verticalLayout")
        self.verticalSpacer = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout.addItem(self.verticalSpacer)

        self.label_4 = QLabel(self.widget_2)
        self.label_4.setObjectName(u"label_4")
        self.label_4.setMinimumSize(QSize(40, 30))
        self.label_4.setMaximumSize(QSize(40, 30))
        self.label_4.setPixmap(QPixmap(u":/mini-vantage/img/cam.png"))
        self.label_4.setScaledContents(True)

        self.verticalLayout.addWidget(self.label_4, 0, Qt.AlignmentFlag.AlignHCenter)

        self.label_2 = QLabel(self.widget_2)
        self.label_2.setObjectName(u"label_2")
        font = QFont()
        font.setPointSize(12)
        font.setKerning(True)
        self.label_2.setFont(font)
        self.label_2.setScaledContents(False)

        self.verticalLayout.addWidget(self.label_2, 0, Qt.AlignmentFlag.AlignHCenter)

        self.camera_btn = QPushButton(self.widget_2)
        self.camera_btn.setObjectName(u"camera_btn")
        self.camera_btn.setMinimumSize(QSize(125, 40))
        self.camera_btn.setMaximumSize(QSize(125, 40))
        font1 = QFont()
        font1.setPointSize(12)
        font1.setBold(True)
        self.camera_btn.setFont(font1)
        self.camera_btn.setStyleSheet(u"QPushButton{\n"
"	color:rgb(255, 255, 255);\n"
"	background-color: rgb(32, 61, 146);\n"
"}\n"
"\n"
"QPushButton:checked {\n"
"	color: rgb(0, 0, 0);\n"
"	\n"
"	background-color: rgb(255, 255, 255);\n"
"}")
        self.camera_btn.setCheckable(True)
        self.camera_btn.setChecked(False)

        self.verticalLayout.addWidget(self.camera_btn, 0, Qt.AlignmentFlag.AlignHCenter)

        self.verticalSpacer_2 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout.addItem(self.verticalSpacer_2)


        self.gridLayout.addWidget(self.widget_2, 1, 0, 1, 1)

        self.widget_3 = QWidget(Widget)
        self.widget_3.setObjectName(u"widget_3")
        self.widget_3.setMaximumSize(QSize(200, 200))
        self.widget_3.setStyleSheet(u"background-color: rgb(59, 85, 175);\n"
"border-radius: 20px;")
        self.verticalLayout_2 = QVBoxLayout(self.widget_3)
        self.verticalLayout_2.setObjectName(u"verticalLayout_2")
        self.verticalSpacer_3 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_2.addItem(self.verticalSpacer_3)

        self.label_8 = QLabel(self.widget_3)
        self.label_8.setObjectName(u"label_8")
        self.label_8.setMinimumSize(QSize(40, 30))
        self.label_8.setMaximumSize(QSize(40, 30))
        self.label_8.setPixmap(QPixmap(u":/mini-vantage/img/key.png"))
        self.label_8.setScaledContents(True)

        self.verticalLayout_2.addWidget(self.label_8, 0, Qt.AlignmentFlag.AlignHCenter)

        self.label_7 = QLabel(self.widget_3)
        self.label_7.setObjectName(u"label_7")
        self.label_7.setFont(font)
        self.label_7.setScaledContents(False)

        self.verticalLayout_2.addWidget(self.label_7, 0, Qt.AlignmentFlag.AlignHCenter)

        self.fn_btn = QPushButton(self.widget_3)
        self.fn_btn.setObjectName(u"fn_btn")
        self.fn_btn.setMinimumSize(QSize(125, 40))
        self.fn_btn.setMaximumSize(QSize(125, 40))
        self.fn_btn.setFont(font1)
        self.fn_btn.setStyleSheet(u"QPushButton{\n"
"	color:rgb(255, 255, 255);\n"
"	background-color: rgb(32, 61, 146);\n"
"}\n"
"\n"
"QPushButton:checked {\n"
"	color: rgb(0, 0, 0);\n"
"	\n"
"	background-color: rgb(255, 255, 255);\n"
"}")
        self.fn_btn.setCheckable(True)
        self.fn_btn.setChecked(False)

        self.verticalLayout_2.addWidget(self.fn_btn, 0, Qt.AlignmentFlag.AlignHCenter)

        self.verticalSpacer_4 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_2.addItem(self.verticalSpacer_4)


        self.gridLayout.addWidget(self.widget_3, 1, 1, 1, 1)

        self.widget_5 = QWidget(Widget)
        self.widget_5.setObjectName(u"widget_5")
        self.widget_5.setMaximumSize(QSize(200, 200))
        self.widget_5.setStyleSheet(u"background-color: rgb(59, 85, 175);\n"
"border-radius: 20px;")
        self.verticalLayout_4 = QVBoxLayout(self.widget_5)
        self.verticalLayout_4.setObjectName(u"verticalLayout_4")
        self.verticalSpacer_7 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_4.addItem(self.verticalSpacer_7)

        self.label_10 = QLabel(self.widget_5)
        self.label_10.setObjectName(u"label_10")
        self.label_10.setMinimumSize(QSize(40, 30))
        self.label_10.setMaximumSize(QSize(40, 30))
        self.label_10.setPixmap(QPixmap(u":/mini-vantage/img/usb.png"))
        self.label_10.setScaledContents(True)

        self.verticalLayout_4.addWidget(self.label_10, 0, Qt.AlignmentFlag.AlignHCenter)

        self.label_9 = QLabel(self.widget_5)
        self.label_9.setObjectName(u"label_9")
        self.label_9.setFont(font)
        self.label_9.setScaledContents(False)

        self.verticalLayout_4.addWidget(self.label_9, 0, Qt.AlignmentFlag.AlignHCenter)

        self.usb_btn = QPushButton(self.widget_5)
        self.usb_btn.setObjectName(u"usb_btn")
        self.usb_btn.setMinimumSize(QSize(125, 40))
        self.usb_btn.setMaximumSize(QSize(100, 40))
        self.usb_btn.setFont(font1)
        self.usb_btn.setStyleSheet(u"QPushButton{\n"
"	color:rgb(255, 255, 255);\n"
"	background-color: rgb(32, 61, 146);\n"
"}\n"
"\n"
"QPushButton:checked {\n"
"	color: rgb(0, 0, 0);\n"
"	\n"
"	background-color: rgb(255, 255, 255);\n"
"}")
        self.usb_btn.setCheckable(True)
        self.usb_btn.setChecked(False)

        self.verticalLayout_4.addWidget(self.usb_btn, 0, Qt.AlignmentFlag.AlignHCenter)

        self.verticalSpacer_8 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_4.addItem(self.verticalSpacer_8)


        self.gridLayout.addWidget(self.widget_5, 1, 3, 1, 1)

        self.widget_1 = QWidget(Widget)
        self.widget_1.setObjectName(u"widget_1")
        self.widget_1.setMaximumSize(QSize(16777215, 200))
        self.widget_1.setStyleSheet(u"background-color: rgb(59, 85, 175);\n"
"border-radius: 20px;")
        self.horizontalLayout_2 = QHBoxLayout(self.widget_1)
        self.horizontalLayout_2.setObjectName(u"horizontalLayout_2")
        self.label = QLabel(self.widget_1)
        self.label.setObjectName(u"label")
        font2 = QFont()
        font2.setPointSize(24)
        font2.setBold(True)
        font2.setItalic(False)
        font2.setUnderline(False)
        font2.setStrikeOut(False)
        self.label.setFont(font2)
        self.label.setScaledContents(True)
        self.label.setAlignment(Qt.AlignmentFlag.AlignCenter)

        self.horizontalLayout_2.addWidget(self.label)


        self.gridLayout.addWidget(self.widget_1, 0, 0, 1, 2)

        self.widget_4 = QWidget(Widget)
        self.widget_4.setObjectName(u"widget_4")
        self.widget_4.setMaximumSize(QSize(200, 200))
        self.widget_4.setStyleSheet(u"background-color: rgb(59, 85, 175);\n"
"border-radius: 20px;")
        self.verticalLayout_3 = QVBoxLayout(self.widget_4)
        self.verticalLayout_3.setObjectName(u"verticalLayout_3")
        self.verticalSpacer_5 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_3.addItem(self.verticalSpacer_5)

        self.label_6 = QLabel(self.widget_4)
        self.label_6.setObjectName(u"label_6")
        self.label_6.setMinimumSize(QSize(40, 30))
        self.label_6.setMaximumSize(QSize(40, 30))
        self.label_6.setPixmap(QPixmap(u":/mini-vantage/img/bat.png"))
        self.label_6.setScaledContents(True)

        self.verticalLayout_3.addWidget(self.label_6, 0, Qt.AlignmentFlag.AlignHCenter)

        self.label_5 = QLabel(self.widget_4)
        self.label_5.setObjectName(u"label_5")
        self.label_5.setFont(font)
        self.label_5.setScaledContents(False)

        self.verticalLayout_3.addWidget(self.label_5, 0, Qt.AlignmentFlag.AlignHCenter)

        self.bat_btn = QPushButton(self.widget_4)
        self.bat_btn.setObjectName(u"bat_btn")
        self.bat_btn.setMinimumSize(QSize(125, 40))
        self.bat_btn.setMaximumSize(QSize(125, 40))
        self.bat_btn.setFont(font1)
        self.bat_btn.setStyleSheet(u"QPushButton{\n"
"	color:rgb(255, 255, 255);\n"
"	background-color: rgb(32, 61, 146);\n"
"}\n"
"\n"
"QPushButton:checked {\n"
"	color: rgb(0, 0, 0);\n"
"	\n"
"	background-color: rgb(255, 255, 255);\n"
"}")
        self.bat_btn.setCheckable(True)
        self.bat_btn.setChecked(False)

        self.verticalLayout_3.addWidget(self.bat_btn, 0, Qt.AlignmentFlag.AlignHCenter)

        self.verticalSpacer_6 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_3.addItem(self.verticalSpacer_6)


        self.gridLayout.addWidget(self.widget_4, 1, 2, 1, 1)

        self.widget_6 = QWidget(Widget)
        self.widget_6.setObjectName(u"widget_6")
        self.widget_6.setMaximumSize(QSize(16777215, 200))
        self.widget_6.setStyleSheet(u"background-color: rgb(59, 85, 175);\n"
"border-radius: 20px;")
        self.horizontalLayout = QHBoxLayout(self.widget_6)
        self.horizontalLayout.setObjectName(u"horizontalLayout")
        self.widget = QWidget(self.widget_6)
        self.widget.setObjectName(u"widget")
        self.widget.setMinimumSize(QSize(0, 0))
        self.widget.setMaximumSize(QSize(100, 150))
        self.widget.setStyleSheet(u"")
        self.verticalLayout_5 = QVBoxLayout(self.widget)
        self.verticalLayout_5.setObjectName(u"verticalLayout_5")
        self.verticalSpacer_9 = QSpacerItem(0, 20, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_5.addItem(self.verticalSpacer_9)

        self.label_3 = QLabel(self.widget)
        self.label_3.setObjectName(u"label_3")
        self.label_3.setMinimumSize(QSize(40, 40))
        self.label_3.setMaximumSize(QSize(40, 40))
        self.label_3.setPixmap(QPixmap(u":/mini-vantage/img/fan.png"))
        self.label_3.setScaledContents(True)

        self.verticalLayout_5.addWidget(self.label_3, 0, Qt.AlignmentFlag.AlignHCenter)

        self.label_11 = QLabel(self.widget)
        self.label_11.setObjectName(u"label_11")
        font3 = QFont()
        font3.setPointSize(12)
        self.label_11.setFont(font3)

        self.verticalLayout_5.addWidget(self.label_11, 0, Qt.AlignmentFlag.AlignHCenter)

        self.verticalSpacer_10 = QSpacerItem(0, 10, QSizePolicy.Policy.Minimum, QSizePolicy.Policy.Fixed)

        self.verticalLayout_5.addItem(self.verticalSpacer_10)


        self.horizontalLayout.addWidget(self.widget)

        self.widget_7 = QWidget(self.widget_6)
        self.widget_7.setObjectName(u"widget_7")
        self.widget_7.setMinimumSize(QSize(0, 0))
        self.widget_7.setMaximumSize(QSize(200, 150))
        self.widget_7.setStyleSheet(u"")
        self.verticalLayout_6 = QVBoxLayout(self.widget_7)
        self.verticalLayout_6.setObjectName(u"verticalLayout_6")
        self.fan_combo = QComboBox(self.widget_7)
        self.fan_combo.addItem("")
        self.fan_combo.addItem("")
        self.fan_combo.addItem("")
        self.fan_combo.addItem("")
        self.fan_combo.setObjectName(u"fan_combo")
        self.fan_combo.setMinimumSize(QSize(0, 40))
        self.fan_combo.setMaximumSize(QSize(16777215, 40))
        font4 = QFont()
        font4.setPointSize(11)
        font4.setBold(False)
        self.fan_combo.setFont(font4)
        self.fan_combo.setStyleSheet(u"color:rgb(255, 255, 255);\n"
"background-color: rgb(32, 61, 146);")

        self.verticalLayout_6.addWidget(self.fan_combo)

        self.fan_btn = QPushButton(self.widget_7)
        self.fan_btn.setObjectName(u"fan_btn")
        self.fan_btn.setMinimumSize(QSize(0, 40))
        self.fan_btn.setMaximumSize(QSize(16777215, 40))
        self.fan_btn.setFont(font1)
        self.fan_btn.setStyleSheet(u"QPushButton{\n"
"	background-color: rgb(31, 61, 146);\n"
"	color: rgb(255, 255, 255);\n"
"}\n"
"QPushButton:hover{\n"
"	background-color: rgb(255, 255, 255);\n"
"	color:rgb(0, 0, 0)\n"
"}\n"
"QPushButton:pressed{\n"
"	background-color: rgb(128, 0, 0);\n"
"	color: rgb(255, 255, 255);\n"
"}")

        self.verticalLayout_6.addWidget(self.fan_btn)


        self.horizontalLayout.addWidget(self.widget_7)


        self.gridLayout.addWidget(self.widget_6, 0, 2, 1, 2)


        self.retranslateUi(Widget)

        self.fan_combo.setCurrentIndex(0)


        QMetaObject.connectSlotsByName(Widget)
    # setupUi

    def retranslateUi(self, Widget):
        Widget.setWindowTitle(QCoreApplication.translate("Widget", u"Mini Vantage", None))
        self.label_4.setText("")
        self.label_2.setText(QCoreApplication.translate("Widget", u"Camera Power", None))
        self.camera_btn.setText(QCoreApplication.translate("Widget", u"NOT WORK", None))
        self.label_8.setText("")
        self.label_7.setText(QCoreApplication.translate("Widget", u"Fn Lock", None))
        self.fn_btn.setText(QCoreApplication.translate("Widget", u"NOT WORK", None))
        self.label_10.setText("")
        self.label_9.setText(QCoreApplication.translate("Widget", u"USB Charging", None))
        self.usb_btn.setText(QCoreApplication.translate("Widget", u"NOT WORK", None))
        self.label.setText(QCoreApplication.translate("Widget", u"<html><head/><body><p><span style=\" font-size:28pt;\">Mini Vantage</span></p><p><span style=\" font-size:12pt;\">A Lenovo Vantage for Linux</span></p></body></html>", None))
        self.label_6.setText("")
        self.label_5.setText(QCoreApplication.translate("Widget", u"BAT Conservation", None))
        self.bat_btn.setText(QCoreApplication.translate("Widget", u"NOT WORK", None))
        self.label_3.setText("")
        self.label_11.setText(QCoreApplication.translate("Widget", u"Fan mode", None))
        self.fan_combo.setItemText(0, QCoreApplication.translate("Widget", u"Super Silent", None))
        self.fan_combo.setItemText(1, QCoreApplication.translate("Widget", u"Standard", None))
        self.fan_combo.setItemText(2, QCoreApplication.translate("Widget", u"Dust Cleaning", None))
        self.fan_combo.setItemText(3, QCoreApplication.translate("Widget", u"Efficient Thermal Dissipation", None))

        self.fan_btn.setText(QCoreApplication.translate("Widget", u"APPLY", None))
    # retranslateUi

