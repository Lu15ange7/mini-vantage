# This Python file uses the following encoding: utf-8
# Important:
# You need to run the following command to generate the ui_form.py file
#     pyside6-uic form.ui -o ui_form.py, or
#     pyside2-uic form.ui -o ui_form.py
import sys, os
from PySide6.QtWidgets import QApplication, QWidget

from ui_form import Ui_Widget

#   FILES CONECTION   #
camera_power_file = "/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/camera_power"
conservation_mode_file = "/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/conservation_mode"
fn_lock_file = "/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/fn_lock"
usb_charging_file = "/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/usb_charging"
fan_mode_file = "/sys/bus/platform/drivers/ideapad_acpi/VPC2004:00/fan_mode"



#   LOAD WIDGET   #
class Widget(QWidget):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_Widget()
        self.ui.setupUi(self)

    #   LOAD STATUS FROM FILES   #
        self.show_btn_status()

    #   BUTTONS CONECTION   #
        self.ui.camera_btn.clicked.connect(self.toggle_camera_power)
        self.ui.bat_btn.clicked.connect(self.toggle_conservation_mode)
        self.ui.fn_btn.clicked.connect(self.toggle_fn_lock)
        self.ui.usb_btn.clicked.connect(self.toggle_usb_charging)
        self.ui.fan_btn.clicked.connect(self.combo_button_fan_mode)

    #   LOAD STATUS TO BUTTONS   #
    def show_btn_status(self):

        with open(camera_power_file, "r") as camera_power_read:
            camera_power_status = camera_power_read.readline().strip()

        with open(conservation_mode_file, "r") as conservation_mode_read:
            conservation_mode_status = conservation_mode_read.readline().strip()

        with open(fn_lock_file, "r") as fn_lock_read:
            fn_lock_status = fn_lock_read.readline().strip()

        with open(usb_charging_file, "r") as usb_charging_read:
            usb_charging_status = usb_charging_read.readline().strip()

        if camera_power_status == "1":
            self.ui.camera_btn.setChecked(True)
            self.ui.camera_btn.setText("ON")
        elif camera_power_status == "0":
            self.ui.camera_btn.setChecked(False)
            self.ui.camera_btn.setText("OFF")

        if conservation_mode_status == "1":
            self.ui.bat_btn.setChecked(True)
            self.ui.bat_btn.setText("ON")
        elif conservation_mode_status == "0":
            self.ui.bat_btn.setChecked(False)
            self.ui.bat_btn.setText("OFF")

        if fn_lock_status == "1":
            self.ui.fn_btn.setChecked(True)
            self.ui.fn_btn.setText("ON")
        elif fn_lock_status == "0":
            self.ui.fn_btn.setChecked(False)
            self.ui.fn_btn.setText("OFF")
        
        if usb_charging_status == "1":
            self.ui.usb_btn.setChecked(True)
            self.ui.usb_btn.setText("ON")
        elif usb_charging_status == "0":
            self.ui.usb_btn.setChecked(False)
            self.ui.usb_btn.setText("OFF")
        

    #   PUSH BUTTONS    #

    def toggle_camera_power(self):
        with open(camera_power_file, "r") as camera_power_read:
            camera_power_status = camera_power_read.readline().strip()
        
        if camera_power_status == '0':
            os.system(f'echo "1" | pkexec tee {camera_power_file}')
            self.show_btn_status()
        else:
            os.system(f'echo "0" | pkexec tee {camera_power_file}')
            self.show_btn_status()
        
    def toggle_conservation_mode(self):
        with open(conservation_mode_file, "r") as conservation_mode_read:
            conservation_mode_status = conservation_mode_read.readline().strip()
        
        if conservation_mode_status == '0':
            os.system(f'echo "1" | pkexec tee {conservation_mode_file}')
            self.show_btn_status()
        else:
            os.system(f'echo "0" | pkexec tee {conservation_mode_file}')
            self.show_btn_status()
    
    def toggle_fn_lock(self):
        with open(fn_lock_file, "r") as fn_lock_read:
            fn_lock_status = fn_lock_read.readline().strip()

        if fn_lock_status == '0':
            os.system(f'echo "1" | pkexec tee {fn_lock_file}')
            self.show_btn_status()
        else:
            os.system(f'echo "0" | pkexec tee {fn_lock_file}')
            self.show_btn_status()

    
    def toggle_usb_charging(self):
        with open(usb_charging_file, "r") as usb_charging_read:
            usb_charging_status = usb_charging_read.readline().strip()

        if usb_charging_status == '0':
            os.system(f'echo "1" | pkexec tee {usb_charging_file}')
            self.show_btn_status()
        else:
            os.system(f'echo "0" | pkexec tee {usb_charging_file}')
            self.show_btn_status()

    def combo_button_fan_mode(self):
        if self.ui.fan_combo.currentIndex() == 0: # Super Silent
            os.system(f'echo "0" | pkexec tee {fan_mode_file}')
        else:
            if self.ui.fan_combo.currentIndex() == 1: # Standard
                os.system(f'echo "1" | pkexec tee {fan_mode_file}')
            else:
                if self.ui.fan_combo.currentIndex() == 2: # Dust Cleaning
                    os.system(f'echo "2" | pkexec tee {fan_mode_file}')
                else:
                    if self.ui.fan_combo.currentIndex() == 3: # Efficient Thermal Dissipation
                        os.system(f'echo "4" | pkexec tee {fan_mode_file}')
                    else:
                        pass

if __name__ == "__main__":
    app = QApplication(sys.argv)
    widget = Widget()
    widget.show()
    sys.exit(app.exec())
