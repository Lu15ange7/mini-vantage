# Mini-Vantage
It's a simple Lenovo Vantage panel for GNU/Linux writed on Python and QT.

![mini-vantage](preview.png "mini-vantage")

## Dependencies
- `python 3.x` (installed on some GNU/Linux systems, check first if that is installed)
- `pySide6` (from the package management of your GNU/Linux distro or install from pip)
- `acpi` (installed on some GNU/Linux systems, check first if that is installed)
- `polkit` (installed on all GNU/Linux systems)

## Portable Binary

Download binary here:
https://www.pling.com/p/2163755/


## Run from Source

If you have all the dependecies installed, you can run the application by executing the following command:

```
python widget.py
```

## Manual Tools
The project consists in these tools:

### Camera Power
For controlling the camera power, the project includes a tool that allows you to turn the camera on and off. This can be useful for privacy or battery conservation purposes.

### Fn Lock
For controlling the Fn key lock, the project includes a tool that allows you to enable or disable the Fn key lock. This can be useful for users who prefer to have the Fn key function as a standard function key.

### BAT Conservation
For controlling the battery conservation mode, the project includes a tool that allows you to enable or disable the battery conservation mode. This can be useful for extending the battery life of your battery charging cycle (this limit the charging to 60% of battery).

### USB Charging
This tool allows you to control the USB charging behavior of your Lenovo device. You can enable or disable USB charging, which can be useful for conserving battery life or charging other devices.

### Fan Mode
The project includes tools to control the fan mode of your Lenovo device. The available modes are:

- `Super Silent`
This mode prioritizes low noise levels, reducing the fan speed to a minimum while still maintaining adequate cooling.

- `Standard`
This is the default fan mode, balancing cooling performance and noise levels.

- `Dust Cleaning (Only available on some models)`
This mode temporarily increases the fan speed to its maximum in order to clear dust and debris from the system's cooling components.

- `Efficient Thermal Dissipation (Only available on some models)`
This mode optimizes the fan speed and airflow to provide maximum cooling performance, ensuring efficient thermal dissipation for demanding workloads.